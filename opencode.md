# OpenCoDE mirror

The git repository [xplan-deegree](https://gitlab.opencode.de/diplanung/diplancockpit/xplan-backend/xplan-deegree) on [OpenCoDE](https://gitlab.opencode.de) is a mirror of [lat-lon's deegree3 fork](https://github.com/lat-lon/deegree3) for following:

* Branches:
	* xplanbox-deegree3.5
* Tags:
	* xplanbox-deegree*

Mirroring occurs regularly and the built Maven artifacts are available in the [project's repository](https://gitlab.opencode.de/diplanung/diplancockpit/xplan-backend/xplan-deegree/-/packages).